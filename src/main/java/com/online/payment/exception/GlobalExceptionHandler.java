package com.online.payment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.online.payment.constants.AppConstants;

/**
 * @author janbee
 *
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = AccountNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleAccountNotFoundException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.ACCOUNT_NOT_FOUND_STATUS_CODE);
		error.setStatusMsg(AppConstants.ACCOUNT_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = OrderNotfoundException.class)
	public ResponseEntity<ErrorResponse> handleOrderNotfoundException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.ORDER_NOT_FOUND_STATUS_CODE);
		error.setStatusMsg(AppConstants.ORDER_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = NotEnoughBalanceException.class)
	public ResponseEntity<ErrorResponse> handleNotEnoughBalanceException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.NOT_ENOUGH_BALANCE_STATUS_CODE);
		error.setStatusMsg(AppConstants.NOT_ENOUGH_BALANCE);
		return new ResponseEntity<>(error,HttpStatus.OK);
	}
}
