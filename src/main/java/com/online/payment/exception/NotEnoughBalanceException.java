package com.online.payment.exception;

/**
 * @author janbee
 *
 */
public class NotEnoughBalanceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotEnoughBalanceException() {
		super();
	}

	public NotEnoughBalanceException(String message) {
		super(message);
	}
	
	

}
