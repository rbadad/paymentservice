package com.online.payment.exception;

/**
 * @author janbee
 *
 */
public class AccountNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFoundException() {
		super();
	}

	public AccountNotFoundException(String arg0) {
		super(arg0);
	}

}
