package com.online.payment.exception;

/**
 * @author janbee
 *
 */
public class OrderNotfoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderNotfoundException() {
		super();
	}

	public OrderNotfoundException(String message) {
		super(message);
	}

	
}
