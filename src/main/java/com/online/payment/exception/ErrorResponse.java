package com.online.payment.exception;

/**
 * @author janbee
 *
 */
public class ErrorResponse {

	private String statusMsg;
	private String statusCode;
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
}
