package com.online.payment.constants;

/**
 * 
 * @author janbee
 *
 */
public class AppConstants {

	public static final String ACCOUNT_NOT_FOUND = "Account not found";
	public static final String ACCOUNT_NOT_FOUND_STATUS_CODE = "8000";
	
	public static final String PAYMENT_IS_DONE = "Payment is done";
	public static final String PAYMENT_IS_DONE_STATUS_CODE = "3000";
	
	public static final String PAYMENT_IS_NOT_DONE = "Payment is not done";
	public static final String PAYMENT_IS_NOT_DONE_STATUS_CODE = "3001";
	
	public static final String CREDITED = "Credited";
	public static final String TO_VENDOR_ACCOUNT = " to Vendor account";
	
	public static final String ORDER_NOT_FOUND = "Order not found";
	public static final String ORDER_NOT_FOUND_STATUS_CODE = "8001";
	
	public static final String NOT_ENOUGH_BALANCE = "Account is not having enough balance!";
	public static final String NOT_ENOUGH_BALANCE_STATUS_CODE = "8002";
	
	
	public static final String CHECK_AVAIALBILITY_OF_ORDER = "http://localhost:2020/online-foodstore/orders/{orderId}";
	
	private AppConstants() {
		
	}
}
