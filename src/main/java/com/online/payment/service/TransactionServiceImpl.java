package com.online.payment.service;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.online.payment.constants.AppConstants;
import com.online.payment.dto.PaymentRequestDto;
import com.online.payment.dto.ResponseDto;
import com.online.payment.entity.Account;
import com.online.payment.entity.TransactionDetails;
import com.online.payment.exception.AccountNotFoundException;
import com.online.payment.exception.NotEnoughBalanceException;
import com.online.payment.exception.OrderNotfoundException;
import com.online.payment.repository.AccountRepository;
import com.online.payment.repository.TransactionRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService{

	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionServiceImpl.class);
	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public ResponseDto payment(PaymentRequestDto requestDto) throws AccountNotFoundException, OrderNotfoundException, NotEnoughBalanceException {
		LOGGER.info("TransactionServiceImpl :: payment :: start");
		TransactionDetails transaction = new TransactionDetails();
		ResponseDto response = new ResponseDto();
		Integer orderId = requestDto.getOrderId();
		if(!checkAvailabilityOfOrder(orderId)) {
			throw new OrderNotfoundException();
		}
		Optional<Account> account = accountRepository.findById(requestDto.getAccountId());
		if(!account.isPresent()) {
			throw new AccountNotFoundException();
		}
		Account accountObj = account.get();
		double amount = requestDto.getAmount();
		double balance = accountObj.getAccountBalance();
		if(balance<amount) {
			throw new NotEnoughBalanceException();
		}
		transaction.setOrderId(orderId);
		transaction.setTransactionDate(LocalDate.now());
		transaction.setDescription(AppConstants.CREDITED+requestDto.getAmount()+AppConstants.TO_VENDOR_ACCOUNT);
		transaction.setAmount(amount);
		accountObj.setAccountBalance(balance-amount);
		accountRepository.save(accountObj);
		transaction.setAccount(accountObj);
		TransactionDetails tr = transactionRepository.save(transaction);
		if(tr.getTransactionId()==0) {
			response.setStatusCode(AppConstants.PAYMENT_IS_NOT_DONE_STATUS_CODE);
			response.setStatusMsg(AppConstants.PAYMENT_IS_DONE_STATUS_CODE);
			return response;
		}  
		response.setStatusCode(AppConstants.PAYMENT_IS_DONE_STATUS_CODE);
		response.setStatusMsg(AppConstants.PAYMENT_IS_DONE);
		LOGGER.info("TransactionServiceImpl :: payment :: end");
		return response;
	}
	
	private boolean checkAvailabilityOfOrder(Integer orderId) {
		RestTemplate restTemplate = new RestTemplate();
		String url = AppConstants.CHECK_AVAIALBILITY_OF_ORDER;
		return restTemplate.getForObject(url, Boolean.class, orderId);
		
	}

}
