package com.online.payment.service;

import com.online.payment.dto.PaymentRequestDto;
import com.online.payment.dto.ResponseDto;
import com.online.payment.exception.AccountNotFoundException;
import com.online.payment.exception.NotEnoughBalanceException;
import com.online.payment.exception.OrderNotfoundException;

/**
 * 
 * @author janbee
 *
 */
@FunctionalInterface
public interface TransactionService {

	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto
	 * @throws AccountNotFoundException
	 * @throws OrderNotfoundException
	 * @throws NotEnoughBalanceException
	 */
	public ResponseDto payment(PaymentRequestDto requestDto) throws AccountNotFoundException, OrderNotfoundException, NotEnoughBalanceException;
}
