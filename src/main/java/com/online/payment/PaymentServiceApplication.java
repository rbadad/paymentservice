package com.online.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author janbee
 *
 */
@SpringBootApplication
public class PaymentServiceApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(PaymentServiceApplication.class, args);
	}

	public PaymentServiceApplication() {
		super();
	}
	
	

}
