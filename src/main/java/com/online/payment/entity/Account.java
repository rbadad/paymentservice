package com.online.payment.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
public class Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer accountId;
	private Integer accountNumber;
	private double accountBalance;
	private Integer accountTypeId;
	private String accountType;
	@OneToMany(mappedBy = "account")
	private List<TransactionDetails> transaction;
	
	public Account() {
		super();
	}

	public Account(Integer accountId, Integer accountNumber, double accountBalance, Integer accountTypeId,
			String accountType, List<TransactionDetails> transaction) {
		super();
		this.accountId = accountId;
		this.accountNumber = accountNumber;
		this.accountBalance = accountBalance;
		this.accountTypeId = accountTypeId;
		this.accountType = accountType;
		this.transaction = transaction;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Integer getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(Integer accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public List<TransactionDetails> getTransaction() {
		return transaction;
	}

	public void setTransaction(List<TransactionDetails> transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", accountTypeId=" + accountTypeId + ", accountType=" + accountType
				+ ", transaction=" + transaction + "]";
	}
	
	
	
}
