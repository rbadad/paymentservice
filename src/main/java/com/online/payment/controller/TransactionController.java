package com.online.payment.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.payment.dto.PaymentRequestDto;
import com.online.payment.dto.ResponseDto;
import com.online.payment.exception.AccountNotFoundException;
import com.online.payment.exception.NotEnoughBalanceException;
import com.online.payment.exception.OrderNotfoundException;
import com.online.payment.service.TransactionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author janbee
 *
 */
@RestController
@RequestMapping("/transactions")
@Api("Operations pertaining to transactions")
public class TransactionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);
	@Autowired
	TransactionService transactionService;
	
	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto
	 * @throws AccountNotFoundException
	 * @throws OrderNotfoundException
	 * @throws NotEnoughBalanceException
	 */
	@PostMapping
	@ApiOperation(" payment for orders")
	@ApiResponses({@ApiResponse(code = 8000,message = "Account not found"),
		@ApiResponse(code = 8001,message = "Order not found"),
		@ApiResponse(code = 8002,message = "Account is not having enough balance!")})
	public ResponseEntity<ResponseDto> payment(@RequestBody PaymentRequestDto requestDto) throws AccountNotFoundException, OrderNotfoundException, NotEnoughBalanceException {
		LOGGER.debug("TransactionController :: payment :: start");
		ResponseDto response = transactionService.payment(requestDto);
		LOGGER.debug("TransactionController :: payment :: end");
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
}
