package com.online.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.payment.entity.TransactionDetails;

/**
 * @author janbee
 *
 */
@Repository
public interface TransactionRepository extends JpaRepository<TransactionDetails, Integer>{

}
