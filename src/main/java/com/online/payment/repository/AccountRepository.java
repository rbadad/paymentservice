package com.online.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.payment.entity.Account;

/**
 * @author janbee
 *
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

}
