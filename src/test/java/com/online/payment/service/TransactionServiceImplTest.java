package com.online.payment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.payment.constants.AppConstants;
import com.online.payment.dto.PaymentRequestDto;
import com.online.payment.dto.ResponseDto;
import com.online.payment.entity.Account;
import com.online.payment.entity.TransactionDetails;
import com.online.payment.exception.AccountNotFoundException;
import com.online.payment.exception.NotEnoughBalanceException;
import com.online.payment.exception.OrderNotfoundException;
import com.online.payment.repository.AccountRepository;
import com.online.payment.repository.TransactionRepository;

class TransactionServiceImplTest {

	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	@Mock
	AccountRepository accountRepository;
	@Mock
	TransactionRepository transactionRepository;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPayment() throws OrderNotfoundException, NotEnoughBalanceException {
		PaymentRequestDto request = new PaymentRequestDto();
		request.setAccountId(1);
		request.setAmount(800);
		request.setOrderId(1);
		
		Account account = new Account();
		account.setAccountId(1);
		account.setAccountBalance(10000);
		Optional<Account> accObj = Optional.of(account);
		when(accountRepository.findById(request.getAccountId())).thenReturn(accObj);
		
		TransactionDetails transaction = new TransactionDetails();
		transaction.setOrderId(request.getOrderId());
		transaction.setTransactionDate(LocalDate.now());
		transaction.setDescription(AppConstants.CREDITED+request.getAmount()+AppConstants.TO_VENDOR_ACCOUNT);
		transaction.setAmount(request.getAmount());
		//accountObj.setAccountBalance(balance-amount);
		//accountRepository.save(accountObj);
		transaction.setAccount(account);
		TransactionDetails transaction1 = new TransactionDetails();
		transaction1.setTransactionId(1);
		when(transactionRepository.save(transaction)).thenReturn(transaction1);
		
		ResponseDto response = new ResponseDto();
		response.setStatusCode(AppConstants.PAYMENT_IS_DONE_STATUS_CODE);
		response.setStatusMsg(AppConstants.PAYMENT_IS_DONE);
		
		assertEquals("3000", response.getStatusCode());
		/*
		 * try {
		 * doReturn(accObj).when(accountRepository).findById(request.getAccountId());
		 * transactionServiceImpl.payment(request); }catch(AccountNotFoundException e )
		 * { e.printStackTrace(); }
		 */
	}
	
	@Test
	public void testPayment1() throws OrderNotfoundException, NotEnoughBalanceException {
		PaymentRequestDto request = new PaymentRequestDto();
		request.setAccountId(1);
		request.setAmount(800);
		request.setOrderId(1);
		
		Account account = new Account();
		account.setAccountId(1);
		account.setAccountBalance(10000);
		Optional<Account> accObj = Optional.of(account);
		when(accountRepository.findById(request.getAccountId())).thenReturn(accObj);
		
		TransactionDetails transaction = new TransactionDetails();
		transaction.setOrderId(request.getOrderId());
		transaction.setTransactionDate(LocalDate.now());
		transaction.setDescription(AppConstants.CREDITED+request.getAmount()+AppConstants.TO_VENDOR_ACCOUNT);
		transaction.setAmount(request.getAmount());
		//accountObj.setAccountBalance(balance-amount);
		//accountRepository.save(accountObj);
		transaction.setAccount(account);
		TransactionDetails transaction1 = new TransactionDetails();
		when(transactionRepository.save(transaction)).thenReturn(transaction1);
		
		ResponseDto response = new ResponseDto();
		response.setStatusCode(AppConstants.PAYMENT_IS_NOT_DONE_STATUS_CODE);
		response.setStatusMsg(AppConstants.PAYMENT_IS_NOT_DONE);
		
		assertEquals("3001", response.getStatusCode());
		/*
		 * try {
		 * doReturn(accObj).when(accountRepository).findById(request.getAccountId());
		 * transactionServiceImpl.payment(request); }catch(AccountNotFoundException e )
		 * { e.printStackTrace(); }
		 */
	}
	
	
}
