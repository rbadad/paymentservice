package com.online.payment.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.payment.dto.PaymentRequestDto;
import com.online.payment.dto.ResponseDto;
import com.online.payment.exception.AccountNotFoundException;
import com.online.payment.exception.NotEnoughBalanceException;
import com.online.payment.exception.OrderNotfoundException;
import com.online.payment.service.TransactionService;

class TransactionControllerTest {

	@InjectMocks
	TransactionController transactionController;
	@Mock
	TransactionService transactionService;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPayment() throws AccountNotFoundException, OrderNotfoundException, NotEnoughBalanceException {
		PaymentRequestDto request = new PaymentRequestDto();
		request.setAccountId(1);
		request.setAmount(800);
		request.setOrderId(1);
		
		ResponseDto response = new ResponseDto();
		response.setStatusCode("3000");
		response.setStatusMsg("Payment is done");
		
		doReturn(response).when(transactionService).payment(request);
		transactionController.payment(request);
		assertEquals("3000", response.getStatusCode());
		
	}
}
